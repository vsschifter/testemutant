Segue instruções para iniciar o projeto solicitado.
1º execute pelo console acessando a pasta do projeto o seguinte comando:
docker pull vsschifter/teste-mutant:latest
2º inicie o container com o seguinte comando:
docker run -tdi --name testemutant -p 8080:8080 vsschifter/teste-mutant:latest
3º acesse http://localhost:8080 para visualizar a página inicial
