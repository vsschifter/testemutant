import fetch from "node-fetch";

function getUsers() {
  return fetch('https://jsonplaceholder.typicode.com/users');
}

export default getUsers