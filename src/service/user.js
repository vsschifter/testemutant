import express from "express"
import getUsers from "../repository/user.js"

async function getWebsites() {
  let data = await getUsers();
  data = await data.json();
  const websites = data.map((data) => data.website);
  return websites;
}

async function getSorted() {
  let data = await getUsers();
  data = await data.json();
  return data.sort((a, b) => {
    if (a.name === b.name) {
      if (a.email === b.email) {
        if (a.company.name === b.company.name) {
          return 0
        } else if (a.company.name < b.company.name) {
          return -1
        }
        return 1
      } else if (a.email < b.email) {
        return -1
      }
      return 1
    } else if (a.name < b.name) {
      return -1
    };
    return 1
  });
  return sortedUsersEmailCompany
}

async function getSuiteFilter() {
  let data = await getUsers();
  data = await data.json();
  return data.filter((data) =>
    data.address.suite && data.address.suite.toUpperCase().includes("SUITE")
  );
  return suiteFilter
}


export { getWebsites }
export { getSuiteFilter }
export { getSorted }