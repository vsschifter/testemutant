import express from "express"
import { getWebsites, getSuiteFilter, getSorted } from "./service/user.js"

const app = express();

app.get('/', async (req, res) => {
  res.send('Para acessar somente os websites: http://localhost:8080/websites<br> Para acessar a lista em ordem alfabética: http://localhost:8080/sorted<br>Para acessar a lista com todos que possuem "suite" no endereço: http://localhost:8080/suiteFilter')
})

app.get('/websites', async (req, res, next) => {
  try {
    let websites =
      await getWebsites()
    res.json(websites)
  } catch (err) {
    next(err.stack)
  }
});

app.get('/sorted', async (req, res, next) => {
  try {
    let sorted =
      await getSorted()
    res.json(sorted)
  } catch (err) {
    next(err.stack)
  }
});

app.get('/suiteFilter', async (req, res, next) => {
  try {
    let suiteFilter =
      await getSuiteFilter()
    res.json(suiteFilter)
  } catch (err) {
    next(err.stack)
  }
});

const PORT = 8080;
const HOST = '0.0.0.0'

app.listen(PORT, HOST, () => console.log(`Runing on http://localhost:${PORT}`));